require 'parse-ruby-client'

namespace :reserves do
  desc "Jobs referentes a reservas sem pedido"
  
  task :reserves_timeout_task => :environment do
    date = DateTime.now - 2.hours
    puts "***************************************reserves_timeout_task***************************************************"
    puts "Date: #{date}"
    reservelessons = Reservelesson.where("orderpay_id is null and created_at < ?", date).all

    reservelessons.each do |reserve|
      user = reserve.user
      puts "cancel reserve #{reserve.id}"
      reserve.destroy
    end
    
    puts "FIM ***************************************reserves_timeout_task***************************************************"
  end

end