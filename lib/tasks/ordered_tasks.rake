require 'parse-ruby-client'

namespace :ordered do
  desc "Jobs referentes ao pedido"
  
  task :ordered_timeout_task => :environment do
    date = DateTime.now - 1.hour
    date = date - 15.minutes
    puts "***************************************ordered_timeout_task***************************************************"
    puts "Date: #{date}"
    client = Parse.create(
      :application_id => WEE_CONFIG['PARSE_APP_ID'], 
      :api_key        => WEE_CONFIG['PARSE_API_KEY'], 
      :quiet      => false)
    orders = Orderpay.where("status = ? and (idpagto is null or idpagto = '') and created_at < ?", 0, date).all
    orders.each do |order|
      user = nil
      puts "order #{order.id}"
      
      #verifica se foi realizado o pagamento
      if payment_approved(order) then
        conf_reserves(user, order)
      else
        cancel_reserves(user, order)
      end
      puts "----"
    end
    
    puts "FIM ***************************************ordered_timeout_task***************************************************"
  end

  def payment_approved(order)
    transactions = PagSeguro::Query.find(WEE_CONFIG['PG_EMAIL'], WEE_CONFIG['PG_SECRET'], initial_date: order.created_at-1.minute, final_date: order.created_at+1.hour)
    transactions.each do |transaction|
      if transaction.reference.to_s == order.id.to_s then
        if transaction.status.to_i == 3 then # status pago
          return true
        end
      end
    end
    
    return false
  end
  
  def conf_reserves(user, order)    
    
    require 'parse-ruby-client'
    
    client = Parse.create(
      :application_id => WEE_CONFIG['PARSE_APP_ID'], 
      :api_key        => WEE_CONFIG['PARSE_API_KEY'], 
      :quiet      => false)
      
    msg = ""
    screen = -1
    
    TeacherMailer.confirmation_lessons(order).deliver_later
    UserMailer.payment_conf(user).deliver_later
    msg = "O pagamento de suas aulas reservadas no Wee App foi confirmado."
    screen = WEE_CONFIG['SCREEN_ANDROID_NEXT_LESSONS']

    if not user.device_token.nil? and not user.device_token.blank? then
      data = { :alert => msg, :screen => screen  }
      push = client.push(data)
      query = client.query(Parse::Protocol::CLASS_INSTALLATION).eq('deviceToken', user.device_token)
      push.type = "android"
      push.where = query.where    
      push.save
    end
  end
  
  def cancel_reserves(user, order)
    require 'parse-ruby-client'
    
    client = Parse.create(
      :application_id => WEE_CONFIG['PARSE_APP_ID'], 
      :api_key        => WEE_CONFIG['PARSE_API_KEY'], 
      :quiet      => false)
      
    order.reservelessons.each do |reserve|
      user = reserve.user
      puts "reserve #{reserve.id}"
      reserve.notification_sent = true
      reserve.save
      reserve.destroy
    end
    if not user.nil? then
      UserMailer.payment_not_conf(user).deliver_later
      msg = "O pagamento de suas aulas reservadas no Wee App não foi realizado. Com isso, suas reservas foram canceladas. Por favor, acesse o aplicativo e tente novamente."
      if not user.device_token.nil? and not user.device_token.blank? then
        data = { :alert => msg }
        push = client.push(data)
        query = client.query(Parse::Protocol::CLASS_INSTALLATION).eq('deviceToken', user.device_token)
        push.type = "android"
        push.where = query.where    
        push.save
      end
    end
  end
end