require 'parse-ruby-client'

namespace :rating do
  desc "Jobs referentes a avaliação"
  
  task :rating_task => :environment do
    date = DateTime.now - 45.hour
    client = Parse.create(
      :application_id => WEE_CONFIG['PARSE_APP_ID'], 
      :api_key        => WEE_CONFIG['PARSE_API_KEY'], 
      :quiet      => false)
    reservelessons = Reservelesson.joins(:orderpay).joins(:lesson).where("status = ? and rate = ? and date < ? and (notification_sent = ? or notification_sent is null)", 1, 0, date, false).all
    reservelessons.each do |reserve|
      user = reserve.user
      if not user.nil? then
        
        msg = "Oi! Quando tiver um tempinho, não esqueça de avaliar a sua aula no Wee App."
        if not user.device_token.nil? and not user.device_token.blank? then
          data = { :alert => msg, :screen => WEE_CONFIG['SCREEN_ANDROID_LAST_LESSONS'] }
          push = client.push(data)
          query = client.query(Parse::Protocol::CLASS_INSTALLATION).eq('deviceToken', user.device_token)
          push.type = "android"
          push.where = query.where    
          push.save
          reserve.notification_sent = true
          reserve.save!
        end

      end
    end
  end

end