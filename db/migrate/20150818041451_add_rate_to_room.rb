class AddRateToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :rate, :integer
  end
end
