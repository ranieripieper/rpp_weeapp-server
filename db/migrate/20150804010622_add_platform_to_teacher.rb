class AddPlatformToTeacher < ActiveRecord::Migration
  def change
    add_column :teacher_platforms, :teacher_id, :integer
    add_index :teacher_platforms, :teacher_id
    add_column :teacher_platforms, :platform_id, :integer
    add_index :teacher_platforms, :platform_id
  end
end
