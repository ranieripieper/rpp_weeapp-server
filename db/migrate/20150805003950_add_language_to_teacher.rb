class AddLanguageToTeacher < ActiveRecord::Migration
  def change
    add_column :teacher_languages, :teacher_id, :integer
    add_index :teacher_languages, :teacher_id
    add_column :teacher_languages, :language_id, :integer
    add_index :teacher_languages, :language_id
  end
end
