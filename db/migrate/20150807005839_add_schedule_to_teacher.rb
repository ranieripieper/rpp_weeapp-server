class AddScheduleToTeacher < ActiveRecord::Migration
  def change
    add_column :teacher_schedules, :teacher_id, :integer
    add_index :teacher_schedules, :teacher_id
    add_column :teacher_schedules, :schedule_id, :integer
    add_index :teacher_schedules, :schedule_id
  end
end
