class InsertDefaultValues < ActiveRecord::Migration
  def change
    Level.create :id => 1, :name => "Básico" 
    Level.create :id => 2, :name => "Intermediário" 
    Level.create :id => 3, :name => "Avançado"
    Level.create :id => 4, :name => "Conversação"
    
    Language.create :id => 1, :name => "Inglês"
    Language.create :id => 2, :name => "Espanhol"
    Language.create :id => 3, :name => "Português"
    
  end
end
