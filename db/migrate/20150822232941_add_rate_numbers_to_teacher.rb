class AddRateNumbersToTeacher < ActiveRecord::Migration
  def change
    add_column :teachers, :rate_numbers, :integer, default: 0
  end
end
