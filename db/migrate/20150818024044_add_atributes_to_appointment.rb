class AddAtributesToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :teacher_level_id, :integer
    add_column :appointments, :teacher_platform_id, :integer
    add_column :appointments, :teacher_schedule_id, :integer
  end
end
