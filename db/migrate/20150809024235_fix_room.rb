class FixRoom < ActiveRecord::Migration
  def change
    remove_column :rooms, :room_id
    add_column :rooms, :appointment_id, :integer
  end
end
