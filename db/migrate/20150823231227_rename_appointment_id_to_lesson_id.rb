class RenameAppointmentIdToLessonId < ActiveRecord::Migration
  def change
    rename_column :reserves, :appointment_id, :lesson_id
  end
end
