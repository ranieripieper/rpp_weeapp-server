class AddLanguageToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :teacher_language_id, :integer
  end
end
