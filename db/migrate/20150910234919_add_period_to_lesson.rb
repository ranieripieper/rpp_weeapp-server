class AddPeriodToLesson < ActiveRecord::Migration
  def change
    add_column :lessons, :period, :integer
    
    drop_table :schedules
    drop_table :teacher_schedules
  end
end
