class AddRateToTeacher < ActiveRecord::Migration
  def change
    add_column :teachers, :rate, :integer, default: 0
  end
end
