class RenameRoomIdToReserveId < ActiveRecord::Migration
  def change
    rename_column :orders, :room_id, :reserve_id
  end
end
