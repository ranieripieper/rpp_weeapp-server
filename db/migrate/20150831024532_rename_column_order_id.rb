class RenameColumnOrderId < ActiveRecord::Migration
  def change
    rename_column :reserves, :order_id, :orderpay_id
  end
end
