class AddLanguagelevels < ActiveRecord::Migration

  def change
    create_table :language_levels do |t|
      t.integer :teacher_languages_id
      t.integer :level_id
    end
  end

end
