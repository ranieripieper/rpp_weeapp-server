class AddDeletedAtToReservelesson < ActiveRecord::Migration
  def change
    add_column :reservelessons, :deleted_at, :datetime
    add_index :reservelessons, :deleted_at
  end
end
