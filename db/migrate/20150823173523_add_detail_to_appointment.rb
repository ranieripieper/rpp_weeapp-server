class AddDetailToAppointment < ActiveRecord::Migration
  def change
    add_column :appointments, :level_id, :integer
    add_column :appointments, :language_id, :integer
  end
end
