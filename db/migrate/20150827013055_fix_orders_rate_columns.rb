class FixOrdersRateColumns < ActiveRecord::Migration
  def change
    change_column :reserves, :rate, :integer, default: 0
  end
end
