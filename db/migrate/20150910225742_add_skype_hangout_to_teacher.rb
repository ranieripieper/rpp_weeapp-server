class AddSkypeHangoutToTeacher < ActiveRecord::Migration
  def change
    add_column :teachers, :skype, :string
    add_column :teachers, :hangout, :string
  end
end
