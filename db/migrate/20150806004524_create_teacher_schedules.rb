class CreateTeacherSchedules < ActiveRecord::Migration
  def change
    create_table :teacher_schedules do |t|

      t.timestamp null: false
    end
  end
end
