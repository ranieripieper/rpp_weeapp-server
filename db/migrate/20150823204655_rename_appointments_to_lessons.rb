class RenameAppointmentsToLessons < ActiveRecord::Migration
  def change
    rename_table :appointments, :lessons
  end
end