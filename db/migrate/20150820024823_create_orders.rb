class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :room_id
      t.integer :status
      t.string :idpagto

      t.timestamps null: false
    end
  end
end
