class RemoveScheduleFromAppointment < ActiveRecord::Migration
  def change
    remove_column :appointments, :schedule_id
  end
end
