class CreateTeacherLanguages < ActiveRecord::Migration
  def change
    create_table :teacher_languages do |t|

      t.timestamps null: false
    end
  end
end
