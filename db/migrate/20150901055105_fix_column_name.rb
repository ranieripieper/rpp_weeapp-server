class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :language_levels, :teacher_languages_id, :teacher_language_id
  end
end
