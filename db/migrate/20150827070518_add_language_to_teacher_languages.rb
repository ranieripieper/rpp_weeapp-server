class AddLanguageToTeacherLanguages < ActiveRecord::Migration
  def change
    add_column :teacher_languages, :level_id, :integer
  end
end
