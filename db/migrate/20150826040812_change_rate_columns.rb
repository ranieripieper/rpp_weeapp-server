class ChangeRateColumns < ActiveRecord::Migration
  def change
    change_column :teachers, :rate,  :float
  end
end
