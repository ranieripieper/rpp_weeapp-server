class RenameRoomsToReserves < ActiveRecord::Migration
  def change
    rename_table :rooms, :reserves
  end
end
