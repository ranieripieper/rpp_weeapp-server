class AddNotificationSentToReservelesson < ActiveRecord::Migration
  def change
    add_column :reservelessons, :notification_sent, :boolean
  end
end
