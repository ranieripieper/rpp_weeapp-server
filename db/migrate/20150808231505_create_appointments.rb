class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :teacher_id
      t.integer :schedule_id

      t.timestamps null: false
    end
  end
end
