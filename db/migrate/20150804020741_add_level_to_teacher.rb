class AddLevelToTeacher < ActiveRecord::Migration
  def change
    add_column :teacher_levels, :teacher_id, :integer
    add_index :teacher_levels, :teacher_id
    add_column :teacher_levels, :level_id, :integer
    add_index :teacher_levels, :level_id
  end
end
