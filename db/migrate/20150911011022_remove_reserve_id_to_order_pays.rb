class RemoveReserveIdToOrderPays < ActiveRecord::Migration
  def change
    remove_column :orderpays, :reserve_id, :integer
  end
end
