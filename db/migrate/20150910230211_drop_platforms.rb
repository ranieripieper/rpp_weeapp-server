class DropPlatforms < ActiveRecord::Migration
  def change
    drop_table :platforms
    drop_table :teacher_platforms
    remove_column :lessons, :teacher_platform_id, :integer
  end
end
