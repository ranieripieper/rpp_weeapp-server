class AddCpfAddressToTeacher < ActiveRecord::Migration
  def change
    add_column :teachers, :cpf, :string
    add_column :teachers, :street, :string
    add_column :teachers, :number, :string
    add_column :teachers, :state, :string
    add_column :teachers, :city, :string
    add_column :teachers, :neighborhood, :string
    add_column :teachers, :complement, :string
    add_column :teachers, :zip_code, :string    
  end
end
