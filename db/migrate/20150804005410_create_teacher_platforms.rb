class CreateTeacherPlatforms < ActiveRecord::Migration
  def change
    create_table :teacher_platforms do |t|

      t.timestamps null: false
    end
  end
end
