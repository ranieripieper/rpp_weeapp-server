class AddContactToPlatformTeachers < ActiveRecord::Migration
  def change
    add_column :teacher_platforms, :contact, :string
  end
end
