Rails.application.routes.draw do

  root 'landing#index'

  devise_for :admins, path: "admin", 
    controllers: { sessions: "admin/sessions", registrations: 'admin/registrations' },   
    path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', 
        unlock: 'unlock', registration: 'register', sign_up: 'sign_up' } do
  end
  
  devise_for :teachers, path: "teachers", 
    controllers: { sessions: "teachers/sessions", registrations: 'teachers/registrations' },   
    path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', 
        unlock: 'unlock', registration: 'register', sign_up: 'sign_up' } do
  end
  
  scope :admin do
    resources :tips
    get      'teachers'                        , to: 'admin/teachers#index', as: :admin_teachers_index
    get      'teachers/new'                    , to: 'admin/teachers#new', as: :admin_teachers_new
    post     'teachers'                        , to: 'admin/teachers#create', as: :admin_teachers_create
    
    #resources :teachers, :controller => "admin/teachers"
  end
  resources :teachers do
    resources :lessons
  end
  
  patch 'teachers/avatarupload' => 'teachers#avatarupload', as: :teacher_avatar_upload
  
  get 'invalid' => 'landing#invalid', as: :invalid
  get 'mailtest' => 'landing#mailtest', as: :mailtest
  
  get 'settings' => 'teachers#settings', as: :settings

  post 'save_settings' => 'teachers#save_settings', as: :save_settings

  devise_for :users
  resources :users

  namespace :api, constraints: { format: 'json' } do  
    devise_for :users, controllers: {sessions: "api/sessions", registrations: "api/registrations", passowords: "api/passwords"}

    resources :tips, only: [:index]

    post 'users/:id/lessons/remove_reserves.json' => 'reserves#removemany', as: :removereserves

    get     'order/:order_id/verify'                        , to: 'orders#verify', as: :orders_verify
    post    ':user_id/mailtest'                             , to: 'orders#mailtest'
    post    'order/:order_id/pay'                           , to: 'orders#pay',            as: :pay
    get     'confirmation'                                  , to: 'orders#confirmation',   as: :confirmation
    post    'notify'                                        , to: 'orders#notify',   as: :pay_notify

    post    'avatar_upload'                                 , to: 'users#avatar_upload'
    post    'order'                                         , to: 'orders#create'
    
    resources :users do
      get '' => 'users#index', as: :user_index
      get 'reserves' => 'reserves#index', as: :reserves_index
      # /api/users/:user_id/lessons/:lesson_id/reserves
      resources :lessons do
        resources :reserves
      end
      member do  
        get '' => 'users#show', as: :user 
        put 'edit' => 'users#edit', as: :user_edit
      end
    end
    
    post 'users/:id/lessons/reserves.json' => 'reserves#createmany', as: :createreserves
    
    post 'reservelessons/rate' => 'reserves#rate', as: :reserves_rate

    get 'teachers/:id/lessons' => 'lessons#index', as: :teachers_lesson
    get 'teachers/:id' => 'teachers#show', as: :teacher
    get 'teachers' => 'teachers#index', as: :teacher_index
    # get 'search' => 'lessons#search', as: :lesson_search
    
    get 'languages' => 'lists#languages', as: :languages_list
    get 'levels' => 'lists#levels', as: :levels_list
    get 'schedules' => 'lists#schedules', as: :schedules_list
    get 'platforms' => 'lists#platforms', as: :platforms_list
    get 'search' => 'teachers#search', as: :teacher_search

    # get  'reserves' => 'users#index', as: :reserve_index
    # get  'reserves/:id' => 'users#show', as: :reserve
    # post 'reserves' => 'users#new', as: :reserve_new
    
  end
end
