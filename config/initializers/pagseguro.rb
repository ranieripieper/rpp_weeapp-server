# config/initializers/pagseguro.rb
if ENV["RAILS_ENV"] == "production" then
  PagSeguro::Url.environment = :production
else 
  PagSeguro::Url.environment = :sandbox
end