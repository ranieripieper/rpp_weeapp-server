# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->

	jQuery ->
	  $('#lessons').dataTable
	    oLanguage: {
	      sEmptyTable: "Nenhum registro encontrado",
	      sInfo: "Mostrando _START_ até _END_ do total de _TOTAL_ registros",
	      sInfoEmpty: "Mostrando 0 até 0 em 0 registros",
	      sInfoFiltered: "(Filtrados _MAX_ registros)",
	      sInfoPostFix: "",
	      sInfoThousands: ".",
	      sLengthMenu: "_MENU_ Resultados por página",
	      sLoadingRecords: "Carregando...",
	      sProcessing: "Processando...",
	      sZeroRecords: "Nenhum registro encontrado",
	      sSearch: "Pesquisar",
	      oPaginate: {
	        sNext: "Próximo",
	        sPrevious: "Anterior",
	        sFirst: "Primeiro",
	        sLast: "Último"
	      },
	      oAria: {
	        sSortAscending: ": Ordenar colunas de forma ascendente",
	        sSortDescending: ": Ordenar colunas de forma descendente"
	      }
	    }

	    
	jQuery ->
	  $('#admin_teachers').dataTable {
	    oLanguage: {
	      sEmptyTable: "Nenhum registro encontrado",
	      sInfo: "Mostrando _START_ até _END_ do total de _TOTAL_ registros",
	      sInfoEmpty: "Mostrando 0 até 0 em 0 registros",
	      sInfoFiltered: "(Filtrados _MAX_ registros)",
	      sInfoPostFix: "",
	      sInfoThousands: ".",
	      sLengthMenu: "_MENU_ Resultados por página",
	      sLoadingRecords: "Carregando...",
	      sProcessing: "Processando...",
	      sZeroRecords: "Nenhum registro encontrado",
	      sSearch: "Pesquisar",
	      oPaginate: {
	        sNext: "Próximo",
	        sPrevious: "Anterior",
	        sFirst: "Primeiro",
	        sLast: "Último"
	      },
	      oAria: {
	        sSortAscending: ": Ordenar colunas de forma ascendente",
	        sSortDescending: ": Ordenar colunas de forma descendente"
	      }
	    }
	  }
	jQuery ->
	  $('#admin_tips').dataTable {
	    oLanguage: {
	      sEmptyTable: "Nenhum registro encontrado",
	      sInfo: "Mostrando _START_ até _END_ do total de _TOTAL_ registros",
	      sInfoEmpty: "Mostrando 0 até 0 em 0 registros",
	      sInfoFiltered: "(Filtrados _MAX_ registros)",
	      sInfoPostFix: "",
	      sInfoThousands: ".",
	      sLengthMenu: "_MENU_ Resultados por página",
	      sLoadingRecords: "Carregando...",
	      sProcessing: "Processando...",
	      sZeroRecords: "Nenhum registro encontrado",
	      sSearch: "Pesquisar",
	      oPaginate: {
	        sNext: "Próximo",
	        sPrevious: "Anterior",
	        sFirst: "Primeiro",
	        sLast: "Último"
	      },
	      oAria: {
	        sSortAscending: ": Ordenar colunas de forma ascendente",
	        sSortDescending: ": Ordenar colunas de forma descendente"
	      }
	    }
	  }
	  
$(document).ready(ready)
$(document).on('page:load', ready)