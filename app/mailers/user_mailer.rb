class UserMailer < ApplicationMailer
  default from: 'no-reply@wee.com.br'
 
  def send_payment_link(user, link)
    @user = user
    @link = link
    mail(to: @user.email, subject: '[WeeApp] Aulas reservadas!')
  end
  

  def payment_conf(user)
    @user = user
    mail(to: @user.email, subject: '[WeeApp] Pagamento confirmado!')
  end
  
  def payment_not_conf(user)
    @user = user
    mail(to: @user.email, subject: '[WeeApp] Pagamento não realizado!')
  end
  
  def payment_not_approved(user)
    @user = user
    mail(to: @user.email, subject: '[WeeApp] Pagamento não aprovado!')
  end
  
end
