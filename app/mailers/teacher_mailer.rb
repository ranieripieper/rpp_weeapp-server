class TeacherMailer < ApplicationMailer
  default from: 'no-reply@wee.com.br'
 
  def welcome_email(teacher, pwd)
    @teacher = teacher
    @pwd = pwd
    mail(to: @teacher.email, subject: '[WeeApp] Bem-vindo!')
  end
  
  def confirmation_lessons(orderpay)
    @orderpay = orderpay
    
    @reservelessons = orderpay.reservelessons
    @teacher = @reservelessons[0].lesson.teacher
    @user = @reservelessons[0].user
    mail(to: @teacher.email, subject: '[WeeApp] Aulas vendidas!')
  end
end
