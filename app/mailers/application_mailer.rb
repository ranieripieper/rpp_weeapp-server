class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@wee.com.br"
  layout 'mailer'
end