class Admin::TeachersController < ApplicationController
    

    
  before_action :set_teacher, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!

  respond_to :html

  # GET /teachers
  # GET /teachers.json
  def index
    @teachers = Teacher.all.order(name: :asc)
  end

  # GET /teachers/1
  # GET /teachers/1.json
  def show
  end

  # GET /teachers/new
  def new
    @teacher = Teacher.new
  end

  # GET /teachers/1/edit
  def edit
  end

  # POST /teachers
  # POST /teachers.json
  def create
    pwd = SecureRandom.hex(4)
    params[:teacher][:password] = pwd
    params[:teacher]["password_confirmation"] = pwd
    
    @teacher = Teacher.new(teacher_params)

    respond_to do |format|
      if @teacher.save
        #envia email com a senha
        TeacherMailer.welcome_email(@teacher, pwd).deliver
        format.html { redirect_to admin_teachers_index_path, :flash => { :success => "Professor cadastrado com sucesso." } }
        format.json { render :show, status: :created, location: @teacher }
      else
        format.html { render :new }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teachers/1
  # PATCH/PUT /teachers/1.json
  def update
  end

  # DELETE /teachers/1
  # DELETE /teachers/1.json
  def destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teacher
      # @teacher = Teacher.find(params[:id])
      @teacher = current_teacher
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def teacher_params
      params.require(:teacher).permit(:name, :email, :password, "password_confirmation")
    end
end
