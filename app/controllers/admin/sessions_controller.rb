class Admin::SessionsController < Devise::SessionsController

  def after_sign_up_path_for(resource)
    root_path(:msg => "Cadastro efetuado")
  end
  
  def after_sign_in_path_for(resource)
    root_path(:msg => "Login efetuado")
  end
  
  def new
    super
  end
end
