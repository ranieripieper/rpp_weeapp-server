class Api::ListsController < ApplicationController
  respond_to :json
  
  def languages
    @languages = Language.all
  end

  def levels
    @levels = Level.all
  end

  def schedules
    @schedules = Schedule.all
  end

  def platforms
    @platforms = Platform.all
  end

end