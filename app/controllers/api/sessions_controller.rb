class Api::SessionsController < Devise::SessionsController
  
  # skip_before_filter :authenticate_user_from_token
  respond_to :json
  # skip_before_filter :verify_authenticity_token
  skip_before_filter :verify_signed_out_user
  before_filter :authenticate_user_from_token!, only: [:destroy]

  def new
    super
  end

  def destroy
    @user = current_user

    if @user.blank?
      return render status: 400, json: {errors: ['token inválido']}
    else
      @user.update_attribute(:auth_token , '')
      return render status: 200, json: {}
    end
  end

  def create
    device_token = params[:device_token]
    if params[:access_token].present?
      begin
        fb_user = FbGraph2::User.me(params[:access_token]).fetch
      rescue
        return render status: 400, json: {errors: ['access_token inválido']}
      end

      email = params[:email]
      @user = User.where(:facebook_uid => fb_user.raw_attributes[:id]).first

      if @user.blank?
        
        @user = User.find_by_email(email)

        if @user.present?
          @user.update_attributes(:facebook_uid => fb_user.raw_attributes[:id], :device_token => device_token)
        else
          generated_password = Devise.friendly_token.first(8)

          @user = User.create(
            { :name =>fb_user.raw_attributes[:name],
              :email => email,
              :facebook_uid=>fb_user.raw_attributes[:id],
              :avatar => "http://graph.facebook.com/#{fb_user.raw_attributes[:id]}/picture?type=large&width=960",
              :password => generated_password,
              :device_token => device_token
            })
          
        end
        
      else
        @user.device_token = device_token
        @user.email = params[:email]
        @user.save!
      end
      
      User.where("device_token = ? and id != ? ", device_token, @user.id).update_all( "device_token = ''")
      
      sign_in @user, store: false
      
      # redirect_to user_token_api_user_path(@user)
      respond_to do |format|
        format.json { render :json => @user }
      end
    else      

      email = params[:email]
      password = params[:password]
      
      @user = User.where(:email => email).first()
      if not @user.blank?
        if @user.valid_password?(password)
          @user.generate_new_token
          #update users com o mesmo device_token
          User.where("device_token = ? and id != ? ", device_token, @user.id).update_all( "device_token = ''")
          @user.device_token = device_token
          @user.save!
          respond_with(@user)
        else
          warden.custom_failure!
          return render status: 401, json: {errors: ['senha incorreta']}
        end
      else
        return render status: 401, json: {errors: ['id incorreto']}
      end
    end
  end
end