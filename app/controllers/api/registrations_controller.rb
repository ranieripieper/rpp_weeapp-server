class Api::RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def create
    
    @user = User.where(:email => params[:email]).first()
    if @user.blank? 

      @user = User.create(:name => params[:name], :email => params[:email], :device_token => params[:device_token], :password => params[:password], :password_confirmation => params[:password_confirmation])
      
      @user.ensure_authentication_token!
      
      if params[:password].blank? 
        pass = Devise.friendly_token[0,20]
        @user.password = pass
        @user.password_confirmation = pass
      end
        
      if @user.save

        avatar = params[:avatar]

        if avatar.class == ActionDispatch::Http::UploadedFile
          
          file_temp = avatar.tempfile

          if file_temp.present?
            begin
              url = @user.upload_avatar(file_temp)
            rescue
              @user.destroy
              return render status: 400, json: {errors: ['file invalid']}
            end
          end
        else
          @user.update_attributes(:avatar => avatar)
        end

        return respond_with @user
      else
        warden.custom_failure!
        return render status: 422, json: {errors: @user.errors.full_messages}
      end
    else
      return render status: 400, json: {errors: ['user already exists']}
    end
  end

  def new
    super
  end
  
end