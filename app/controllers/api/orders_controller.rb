class Api::OrdersController < ApplicationController
  respond_to :json

  before_filter :authenticate_user_from_token!, only: [:create, :pay]

  before_action :set_order, only: [:pay]

  def mailtest
    @user = User.find(params[:user_id])
    UserMailer.welcome_email(@user).deliver

    return render status: 200, json: {user: @user}
  end

  def verify
    @order = Orderpay.find_by_id(params[:order_id])

    if @order.blank?
      respond_to do |format|
        format.html { redirect_to invalid_path, :flash => { :message => "Sinto muito, o pagamento foi expirado. Efetue a reserva novamente." } }
      end
      return
    end

    if not @order.idpagto.nil? and @order.status == 0 then
      cancel_reserves(@order.id)
      respond_to do |format|
        format.html { redirect_to invalid_path, :flash => { :message => "Sinto muito, o pagamento não foi aprovado. Efetue a reserva novamente." } }
      end
      return
    end
    if not @order.idpagto.nil? and @order.status == 1 then
      respond_to do |format|
        format.html { redirect_to invalid_path, :flash => { :message => "O pagamento já foi realizado." } }
      end
      return
    end
    if 1.hour.ago <= @order.created_at
      respond_to do |format|
        format.html { redirect_to @order.link }
      end
      return
    else
      cancel_reserves(@order.id)
      respond_to do |format|
        format.html { redirect_to invalid_path, :flash => { :message => "Sinto muito, o pagamento não foi aprovado. Efetue a reserva novamente." } }
      end
      return
    end

  end

  def create
    @order = Orderpay.create(:status => 0)

    reserves_id = params[:reserves]

    reserves = reserves_id.split(",")

    lessons = Reservelesson.where({id: reserves})
    pag_payment = PagSeguro::Payment.new(WEE_CONFIG['PG_EMAIL'], WEE_CONFIG['PG_SECRET'], id: @order.id)

    if lessons.present? 
      lessons.each do |reserve|
        reserve.update_attributes(:orderpay_id => @order.id)

        item = PagSeguro::Item.new(id: "#{reserve.id}", description: "Wee App - 25min de aula de #{reserve.lesson.language.name} no dia #{reserve.lesson.date} com o professor #{reserve.lesson.teacher.name}", amount: "25", quantity: "1")

        # @order.update_attributes(:payment_id => @order.id)
        pag_payment.items.push(item)
      end

      @order.update_attributes(:link => pag_payment.checkout_payment_url)
      UserMailer.send_payment_link(current_user, api_orders_verify_path(@order)).deliver_now
      return render status: 200, json: {redirect_url: pag_payment.checkout_payment_url}
    
    else
      @order.destroy
    end

  end

  def pay
    order_id = params[:order_id]
    @order = Orderpay.find(order_id)

    pag_payment = PagSeguro::Payment.new(WEE_CONFIG['PG_EMAIL'], WEE_CONFIG['PG_SECRET'], id: order_id)

    @order.reservelessons.each do |reserve|
      item = PagSeguro::Item.new(id: "#{reserve.id}", description: "aula", amount: "25", quantity: "1")

      # @order.update_attributes(:payment_id => @order.id)
      pag_payment.items.push(item)

    end
    
    UserMailer.send_payment_link(current_user, pag_payment.checkout_payment_url).deliver

    return render status: 200, json: {redirect_url: pag_payment.checkout_payment_url}
  end

  def confirmation
    id_pagseguro = params[:id_pagseguro]
    query = PagSeguro::Query.new(WEE_CONFIG['PG_EMAIL'], 
                                 WEE_CONFIG['PG_SECRET'], 
                                 id_pagseguro)
    
    order = Orderpay.find_by_id(query.id)

    if order.present?
       order.update_attributes(:idpagto => id_pagseguro, :status => query.approved? ? 1 : 0)
       user = order.reservelessons[0].user
       notify_order_status(user, query.approved?, order)
    end

    redirect_to root_url

  end
  
  def notify
    
      
    email = WEE_CONFIG['PG_EMAIL']
    token = WEE_CONFIG['PG_SECRET']
    notification_code = params[:notificationCode]

    notification = PagSeguro::Notification.new(email, token, notification_code)

    order = Orderpay.find_by_id(notification.id)
    if order.present?
      
      status = 0
      if notification.approved? then
        status = 1
      elsif notification.cancelled? || notification.returned? then
        status = 0
      end
      
      order.update_attributes(:idpagto => notification.transaction_id, :status => status)
      if order.reservelessons.length > 0 then
        user = order.reservelessons[0].user
        
        if notification.cancelled? || notification.returned? || notification.approved? then
          notify_order_status(user, notification.approved?, order)
        end
      end
    end

    render :nothing => true
   
  end

  def notify_order_status(user, approved, order)    
    
    require 'parse-ruby-client'
    
    client = Parse.create(
      :application_id => WEE_CONFIG['PARSE_APP_ID'], 
      :api_key        => WEE_CONFIG['PARSE_API_KEY'], 
      :quiet      => false)
      
    msg = ""
    screen = -1
    
    if not approved then
      #cancela reservas
      cancel_reserves(order.id)
      UserMailer.payment_not_approved(user).deliver_later
      msg = "O pagamento de suas aulas reservadas no Wee App não foi aprovado. Com isso, suas reservas foram canceladas. Por favor, acesse o aplicativo e tente novamente."
    else
       TeacherMailer.confirmation_lessons(order).deliver_later
       UserMailer.payment_conf(user).deliver_later
       msg = "O pagamento de suas aulas reservadas no Wee App foi confirmado."
       screen = WEE_CONFIG['SCREEN_ANDROID_NEXT_LESSONS']
    end
    if not user.device_token.nil? and not user.device_token.blank? then
      data = { :alert => msg, :screen => screen  }
      push = client.push(data)
      query = client.query(Parse::Protocol::CLASS_INSTALLATION).eq('deviceToken', user.device_token)
      push.type = "android"
      push.where = query.where    
      push.save
    end
  end

  def cancel_reserves(order_id)
    reserves = Reservelesson.where("orderpay_id = ?", order_id).all
     
    user = nil
    reserves.each do |reserve|
      user = reserve.user
      puts "reserve #{reserve.id}"
      reserve.notification_sent = true
      reserve.save
      reserve.destroy
    end
      
  end
  
  private
    def set_order
      @order = Orderpay.find(params[:order_id])
    end

end