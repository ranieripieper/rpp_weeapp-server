class Api::LessonsController < ApplicationController
  respond_to :json
  before_filter :authenticate_user_from_token!, only: [:index]

  def index
    teacher_id = params[:id]
    date = DateTime.now + 12.hours
    @lessons = Lesson
                  .where("teacher_id = ? and lessons.date > ?", teacher_id, date)
                  .where(" (lessons.id NOT IN (SELECT reservelessons.lesson_id FROM reservelessons where reservelessons.deleted_at IS  NULL))") 
                  .order(date: :asc)
                  
=begin
.joins("LEFT JOIN reservelessons ON reservelessons.lesson_id = lessons.id and reservelessons.deleted_at is null")
    teacher_id = params[:id]

    lessons = Lesson.empty

    ids = []
    lessons.all.each do |lesson| 
      ids << lesson.id
    end

    # @lessons = Lesson.joins(:reserve).where("reserves.user_id = ?",user)
    @lessons = Lesson.joins(:teacher).where(teachers: {id: teacher_id})

    @lessons = @lessons.where({id:ids} )
=end
  end

  def search
    platform = params[:platform]
    level = params[:level]
    language = params[:language]
    schedule = params[:schedule]

    @lessons = Lesson.search(platform, level, language, schedule)

    respond_with(@lessons)
    
  end

end 