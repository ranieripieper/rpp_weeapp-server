require 'ServiceColumns'

class Api::TipsController < ApplicationController
  respond_to :json
  before_filter :authenticate_user_from_token!

  def index
    page = params[:page] || 1
    per_page = params[:per_page] || 1

    @tips = Tip.all.order(created_at: :desc).paginate(:page => page.to_i, :per_page => per_page.to_i)

    @total = {:page => page.to_i, :total => (@tips.count / per_page.to_f).round }
  
    return render :status => 200, :json => {:tips => @tips, :pagination => @total}
  end

end