require 'ServiceColumns'

class Api::ReservesController < ApplicationController
  respond_to :json
  
  #before_action :set_reserve, only: [:rate]
  
  before_filter :authenticate_user_from_token!

  def index
    date = Time.now
    
    filter = params[:filter]
    
    # aulas passadas 
=begin
    if (filter == "old")
      @reserves = Reservelesson.joins(:lesson).where(:user_id => params[:user_id]).where("lessons.date <= ?", date)
    elsif filter == "new"
      date = date - 30.minutes
      @reserves = Reservelesson.joins(:lesson).where(:user_id => params[:user_id]).where("lessons.date >= ?", date)
    else
      @reserves = Reservelesson.joins(:lesson).where(:user_id => params[:user_id])
    end
    @reserves = @reserves.joins(:orderpay).where('orderpays.status = 1')
    @reserves.order("DATE(date)").group("DATE(date)")
=end

    if (filter == "old")
      @lessons = Lesson.joins(:reservelesson).includes(:reservelesson).where("reservelessons.user_id" => params[:user_id]).where("lessons.date <= ?", date)
      @lessons = @lessons.order("lessons.date desc")
    elsif filter == "new"
      date = date - 30.minutes
      @lessons = Lesson.joins(:reservelesson).includes(:reservelesson).where("reservelessons.user_id" => params[:user_id]).where("lessons.date >= ?", date)
      @lessons = @lessons.order("lessons.date asc")
    end
    @lessons = @lessons.joins(:reservelesson => :orderpay).where('orderpays.status = 1')
    
    #byebug
    arr = []
    @lessons = @lessons.group_by { |c| c.date.to_date }.each{|key, value|
      arrx = []
      
      value.each{|item|
        arrx << item#.as_json(ServiceColumns.next_old_lesson)        
      }
      
      arrt = []
      
      arrx.group_by { |c| c.teacher }.each{|k, v|
            arrtx = []
            v.each{|item_teacher|
              arrtx << item_teacher
              
            }
            puts ">>>> k #{k}"
            teacher = k
            teacher.teacher_lessons = arrtx
            arr << { "date" =>  key.to_s.split(" ")[0], "teacher" => teacher.as_json(ServiceColumns.teachers_next_old_lesson) }
            #arrt << { "date" =>  key.to_s.split(" ")[0], "teacher" => arrtx[0]["teacher"], "lessons" => arrtx}
          }
          
      #arr << { "date" => key.to_s.split(" ")[0], "teachers" => arrt}
      #arr << arrt
    }

    return render :status =>200, :json => arr
  end

  def removemany
    reserves = params[:reserves].split(',')

    @reserves = Reservelesson.where(:id => reserves)

    @reserves.all.each do |reserve|
      
      if reserve.orderpay.present?
        # pago status = 1
        if reserve.orderpay.status == 1
          next
        end
        reserve.orderpay.destroy
      end
      puts ">>>> reserve #{reserve.id}"
      @lesson = reserve.lesson
      reserve.destroy
      
      @lesson.update_attributes(:level_id => nil, :language_id => nil)
      
    end

    return render json: []

  end


  def createmany
    
    level = params[:level_id]
    language = params[:language_id]
    
    lessons = params[:lessons].split(',')
    
    lessons.each do |lesson|
      @lesson = Lesson.find(lesson.to_i)

      if @lesson.reservelesson.present?
        next
      end

      @reserve = Reservelesson.new(:lesson => @lesson, :user => current_user, :date => @lesson.date)
      
      if @reserve.save
        @lesson.update_attributes(:level_id => level, :language_id => language)
      end
      
    end
    
    @reserves = Reservelesson.where(:reservelessons => {:lesson_id => lessons})

  end

  def create
    level = params[:level_id]
    language = params[:language_id]

    @lesson = Lesson.find(params[:lesson_id])
    @user = User.find(params[:user_id])

    if @lesson.reservelesson.blank?
      
      @reserve = Reservelesson.new(:lesson => @lesson, :user => @user)

      if @reserve.save
        @lesson.update_attributes(:level_id => level, :language_id => language)
      else
        return render json: @reserve.errors.full_messages, status: :unprocessable_entity
      end
    else
      return render status: 400, json: {errors: ['Aula já está reservada.']}
    end
  end

  def new
    @reserve = Reservelesson.new
  end

  def rate
    ids = params[:reservelessons_id]
    puts ">>>>> #{ids}"
    if ids.nil? 
      render status: 400, json: {errors: ['Reserva não encontrada.']}
    else
      value = params[:rate].to_f
      ids.split(',').each do |id|
        puts ">>>>> #{id}"
        @reserve = Reservelesson.where(:id => id).first
        if @reserve.present? then
          @reserve.rate_teacher(value)
        end
      end
    end

    render status: 200, json: {success: ['Rating.']}
  end

  def edit
  end

  def update
  end

  def destroy
  end
  
  def show
    byebug
  end

  private

    def set_reserve
      @reserve = Reservelesson.find(params[:id])
    end
  
  # Never trust parameters from the scary internet, only allow the white list through.
    def reserve_params
      params.require(:reservelesson).permit(:user_id, :lesson_id, :rate)
    end

end
