require 'ServiceColumns'

class Api::TeachersController < ApplicationController
  respond_to :json
  before_action :set_user, only: [:show]
  
  #before_filter :authenticate_user_from_token!

  # GET /api/teachers.json
  def index
    @teachers = Teacher.all
  end

  # GET /api/teachers/1.json
  def show
    return render :status => 200, :json => @teacher.as_json(ServiceColumns.teachers)
  end

  def search
    page = params[:page] || 1
    per_page = params[:per_page] || 1

    skype = params[:skype]
    hangout = params[:hangout]
    level = params[:level]
    language = params[:language]
    schedule = params[:schedule]

    @teachers = Teacher.search(skype, hangout, level, language, schedule, page, per_page)
    @total = {:page => page.to_i, :total => (@teachers.count / per_page.to_f).round }

    return render :status => 200, :json => {:teachers => @teachers.as_json(ServiceColumns.teachers), :pagination => @total}
  end

  private

    def set_user
      @teacher = Teacher.find(params[:id])
    end
  
  # Never trust parameters from the scary internet, only allow the white list through.
    def teacher_params
      params.require(:teacher).permit(:name)
    end

end
