
class Api::UsersController < ApplicationController
  include ActionController::ImplicitRender
  before_action :set_user, only: [:show, :destroy]

  respond_to :json

  before_filter :authenticate_user_from_token!

  def avatar_upload
    if params[:avatar].present?
      avatar = params[:avatar]

      if avatar.class != ActionDispatch::Http::UploadedFile
        return render status: 400, json: {errors: ['arquivo inválido']}
      end
    
      file_temp = avatar.tempfile

      if file_temp.present?
        begin
          url = current_user.upload_avatar(file_temp)

          return render status: 201, json: {avatar_url: url}
        rescue
          return render status: 400, json: {errors: ['arquivo inválido']}
        end
    
      end
    end

    return render status: 400, json: {errors: ['arquivo não encontrado']}
  end

  def show

  end

  def index
    @users = User.order('created_at DESC')
  end

  def edit

    @user = current_user
    @user.update_attributes(:name => params[:name], 
                                  :email => params[:email], 
                                  :password => params[:password], 
                                  :password_confirmation => params[:password_confirmation])

    avatar = params[:avatar]

    if avatar.class == ActionDispatch::Http::UploadedFile
      
      file_temp = avatar.tempfile

      if file_temp.present?
        begin
          url = @user.upload_avatar(file_temp)
        rescue
          @user.destroy
          return render status: 400, json: {errors: ['arquivo inválido']}
        end
      end
    else
      @user.update_attributes(:avatar => avatar)
    end

    respond_to do |format|
      format.json { render :json => @user }
    end
    # redirect_to user_api_user_path(@user)
  end

  # DELETE /teachers/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :facebook_uid)
    end

end