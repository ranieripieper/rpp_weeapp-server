class Teachers::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]
  #before_filter :sign_out_pre_login, only: [:new]

  def after_sign_up_path_for(resource)
    root_path(:msg => "Cadastro efetuado")
  end
  
  def after_sign_in_path_for(resource)
    root_path(:msg => "Login efetuado")
  end
  
  def sign_out_pre_login
    sign_out(current_teacher)
    sign_out(current_admin)
  end

end
