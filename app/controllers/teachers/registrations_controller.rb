class Teachers::RegistrationsController < Devise::RegistrationsController
# before_filter :configure_sign_up_params, only: [:create]
# before_filter :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end
  
  #force_ssl only: [:new,:create]
  
  skip_before_filter  :verify_authenticity_token

  def edit
    #flash.clear
    super    
  end
  
  def new
    flash.clear
    super
  end
  
=begin
  # POST /resource
  def create
    pwd = SecureRandom.hex(8)
    params[:dentist][:password] = pwd
    params[:dentist]["password_confirmation"] = pwd

    build_resource(sign_up_params) 
    flash.clear
    
    if params["accept_termos"].nil? or params["accept_termos"] != "on" then
      flash["alert alert-danger"] = I18n.t(:error_termos_de_uso_nao_aceito)
      render "new"
    else
      super
      if resource.errors.nil? or resource.errors.empty? then
        value = Dentistas::PaymentHelpers::get_value(resource.plano)
        payment(resource, value, params)
        Thread.new do
          payment = Payment.where(:dentist_id => resource.id).last
          DentistaMailer.payment_process(resource, payment).deliver!
        end
        sign_out
      else
        #erro cadastro não realizado
      end
     end
  end

  def edit
    @dentista = current_dentist
    build_dentista(@dentista)
    super
    flash.clear  
    
  end

  def build_dentista(dentista)
      if dentista.address.nil? then
        dentista.address = Address.new
      end
      if dentista.promocao.nil? then
        dentista.promocao = Promocao.new
      end
      if dentista.at_normal.nil? or dentista.at_normal.size <= 0  then
        dentista.at_normal.build
      end
    
      if dentista.at_emerg.nil? or dentista.at_emerg.size <= 0  then
        dentista.at_emerg.build
      end
    end


  def dentista_params
    params.require(:dentist).permit(:_id, :nome, :site, :email, :profile, :icon, :destaque, :phones => [], :equipe => [], :servicos => [],
    address: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state], promocao: [:perc, :servico], at_normal: [:dia_sem, :h_open, :h_close], at_emerg: [:dia_sem, :h_open, :h_close])
  end
  

  
  def sign_up_params
    params.require(:dentist).permit(:nome, :email, :plano, :password, :password_confirmation, :phones => [])
  end
=end

  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_update_path_for(resource)
    edit_teacher_registration_path(resource)
  end
  
  def update
    super
=begin
    @teacher = current_teacher
    
    params_update = teachers_params

    if params_update[:password].nil? or params_update[:password].blank? then
      params_update[:password] = nil
      params_update[:password_confirmation] = nil
      params_update.delete :password
      params_update.delete :password_confirmation
    end
    
    puts ">>>>>> #{params_update}"
    if @teacher.update(params_update)
      flash["alert alert-success"] = "Alteração efetuada com sucesso."
      
    else
      #set_error(@teacher, "Ocorreu algum erro.")
    end

    
    render 'edit'
=end
  end
  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) << :name
    devise_parameter_sanitizer.for(:account_update) << :avatar
    devise_parameter_sanitizer.for(:account_update) << :skype
    devise_parameter_sanitizer.for(:account_update) << :hangout
    devise_parameter_sanitizer.for(:account_update) << :accepted_term    
    devise_parameter_sanitizer.for(:account_update) << :cpf
    devise_parameter_sanitizer.for(:account_update) << :street
    devise_parameter_sanitizer.for(:account_update) << :number
    devise_parameter_sanitizer.for(:account_update) << :state
    devise_parameter_sanitizer.for(:account_update) << :city
    devise_parameter_sanitizer.for(:account_update) << :neighborhood
    devise_parameter_sanitizer.for(:account_update) << :complement
    devise_parameter_sanitizer.for(:account_update) << :zip_code
    devise_parameter_sanitizer.for(:account_update) << :phone
    
    devise_parameter_sanitizer.for(:sign_up) << :name
  end
  
  def teachers_params
    params.require(:teacher).permit(:id, :name, :email, :password, :password_confirmation)
  end
  
end
