class TipsController < ApplicationController
  before_action :set_tip, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!

  # GET /tips
  # GET /tips.json
  def index
    @tips = Tip.all.order(created_at: :desc)
  end

  # GET /admin/admin/tips/1
  # GET /admin/admin/tips/1.json
  def show
  end

  # GET /admin/admin/tips/new
  def new
    @tip = Tip.new
  end

  # GET /admin/admin/tips/1/edit
  def edit
  end

  # POST /tips
  # POST /tips.json
  def create
    @tip = Tip.new(tip_params)

    respond_to do |format|
      if @tip.save
        send_notification(@tip.description)
        format.html { redirect_to tips_path, :flash => { :success => "Dica inserida com sucesso." } }
        format.json { render :show, status: :created, location: @tip }
      else
        format.html { render :new }
        format.json { render json: @tip.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def send_notification(message)
    require 'parse-ruby-client'

    client = Parse.create(
      :application_id => WEE_CONFIG['PARSE_APP_ID'], 
      :api_key        => WEE_CONFIG['PARSE_API_KEY'], 
      :quiet      => false)

    data = { :alert =>message, :screen => WEE_CONFIG['SCREEN_ANDROID_TIPS'] }
    push = client.push(data)
    push.save
  end

  # PATCH/PUT /admin/tips/1
  # PATCH/PUT /admin/tips/1.json
  def update
    respond_to do |format|
      if @tip.update(tip_params)
        format.html { redirect_to tips_path, :flash => { :success => "Dica alterada com sucesso." } }
        format.json { render :show, status: :ok, location: @tip, :flash => { :success => "Dica alterada com sucesso." } }
      else
        format.html { render :edit }
        format.json { render json: @tip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/admin/tips/1
  # DELETE /admin/admin/tips/1.json
  def destroy
    @tip.destroy
    respond_to do |format|
      format.html { redirect_to tips_url, :flash => { :success => "Dica deletada com sucesso." } }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tip
      @tip = Tip.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tip_params
      params.require(:tip).permit(:description)
    end
end
