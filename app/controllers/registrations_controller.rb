class RegistrationsController < Devise::RegistrationsController
  

  def new

    @languages = Language.all
    @levels = Level.all
    
    super
  end

  def create
    teacher = Teacher.new
    teacher.name = params[:teacher][:name]
    teacher.email = params[:teacher][:email]
    teacher.password = params[:teacher][:password]
    
    if teacher.save
      if teacher.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_in(teacher_name, teacher)
        respond_with teacher, :location => after_sign_up_path_for(teacher)
      else
        set_flash_message :notice, :"signed_up_but_#{teacher.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with teacher, :location => after_inactive_sign_up_path_for(teacher)
      end
    else
      clean_up_passwords teacher
      respond_with teacher
    end

  end

  def update
    super
  end

  private
    def resource_params
      params.require(:teacher).permit(:create, :avatar, :name, :email, :password, :password_confirmation, :current_password)
    end

end 