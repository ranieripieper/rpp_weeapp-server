class TeachersController < ApplicationController
  before_action :set_teacher, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_teacher!, only: [:notificaton, :send_notification, :settings, :new, :index, :update, :destroy]

  respond_to :html

  def avatarupload
    @teacher = current_teacher
    respond_to do |format|
      if @teacher.update(teacherparams)
        format.js { render json: {avatar: true} }
      else
        format.js { render json: @teacher.errors }
      end
    end
  end

  def settings
    @teacher = current_teacher
    @languages = Language.all
    @levels = Level.all

    @language_ids = []
    TeacherLanguage.where(:teacher_id => current_teacher.id).each{|lang| 
      tlevel = {}

      level = []
      if not lang.language_levels.nil? then
        lang.language_levels.each{|ll|
          level << ll.level_id
        }
      end
      if level.present?
        tlevel = {lang.language_id => level}

        @language_ids << tlevel 
      end
    }

    @level_ids = []
    TeacherLevel.where(:teacher_id => current_teacher.id).each{|lev| 
      @level_ids << lev.level_id
    }
    
    TeacherLanguage.joins(:language_levels)

    p @level_ids
  end

  def save_settings
    
    TeacherLanguage.delete_all(:teacher_id => current_teacher.id)
    
    teacher_languages = TeacherLanguage.where(:teacher_id => current_teacher.id)

    LanguageLevel.delete_all(:teacher_language_id => [teacher_languages.map(&:id)])

    
    language_levels = params[:language_ids]

    if not language_levels.nil? then
      language_levels.each do |language_id|
        p "add teacher: #{current_teacher.id} -> language: #{language_id[0]}"
        tlanguage = TeacherLanguage.create(:teacher => current_teacher, :language_id => language_id[0])
        
        language_id[1].each do |level_id|
          p "#{level_id[0]}"
          LanguageLevel.create(:teacher_language_id => tlanguage.id, :level_id => level_id[0])
        end
      end
    end
    flash[:success] = 'Alteração efetuada com sucesso.'
    redirect_to settings_path

  end
  # GET /teachers
  # GET /teachers.json
  def index
    redirect_to teacher_lessons_path(current_teacher)
  end

  # GET /teachers/1
  # GET /teachers/1.json
  def show
  end

  # GET /teachers/new
  def new
    byebug
    @teacher = Teacher.new
    @languages = Language.all
    @levels = Level.all
  end

  # GET /teachers/1/edit
  def edit
    flash.clear
  end

  # POST /teachers
  # POST /teachers.json
  def create
    @teacher = Teacher.new(teacher_params)
    byebug

    teacher_language = TeacherLanguage.where(:level_id => lesson_params[:level_id], :teacher_id => teacher_id, :language_id => lesson_params[:language_id])
    if teacher_language.blank?
      teacher_language = TeacherLanguage.create(:level_id => lesson_params[:level_id], :teacher_id => teacher_id, :language_id => lesson_params[:language_id])
    end
    LanguageLevel.create(:teacher_language_id => teacher_language.id, :level_id => lesson_params[:level_id])

    respond_to do |format|
      if @teacher.save
        flash[:success] = 'Alteração efetuada com sucesso.'
        format.html { redirect_to @teacher}
        format.json { render :show, status: :created, location: @teacher }
      else
        format.html { render :new }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /teachers/1
  # PATCH/PUT /teachers/1.json
  def update
    params_update = teacher_params

    respond_to do |format|
      if @teacher.update(teacher_params)
        flash[:success] = 'Alteração efetuada com sucesso.'
        format.html { redirect_to settings_path }
        format.json { render :show, status: :ok, location: @teacher }
      else
        format.html { render :edit }
        format.json { render json: @teacher.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /teachers/1
  # DELETE /teachers/1.json
  def destroy
    @teacher.destroy
    respond_to do |format|
      format.html { redirect_to teachers_url, notice: 'Teacher was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_teacher
      # @teacher = Teacher.find(params[:id])
      @teacher = current_teacher
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def teacher_params
      params.require(:teacher).permit(:avatar, :name)
    end
end
