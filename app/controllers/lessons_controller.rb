class LessonsController < ApplicationController
  before_action :authenticate_teacher!, only: [:index, :create, :destroy]
  before_action :set_lesson, only: [:destroy]
  respond_to :html
  
  def new
    
    @teacher = Teacher.find(params[:teacher_id])
    @lesson = @teacher.lessons.build
    
    @languages = Language.all
    @levels = Level.all

  end

  def index

    @teacher = Teacher.find(params[:teacher_id])
    @lesson = @teacher.lessons.build
    
    @languages = Language.all
    @levels = Level.all
    
    @lessons = Lesson.where(:teacher => current_teacher)

    @need_add_skype_hangout = false
    @need_add_language = false
    @need_accepted_term = false
    if (@teacher.skype.nil? or @teacher.skype.blank?) and (@teacher.hangout.nil? or @teacher.hangout.blank?) then
      @need_add_skype_hangout = true
    end
    if @teacher.teacher_languages.length <= 0 then
      @need_add_language = true
    end
    if not @teacher.accepted_term then
      @need_accepted_term = true
    end
  end

  def create
    @lesson = Lesson.new(lesson_params)
    
    #verifica a data
    if @lesson.date < DateTime.now
      respond_to do |format|
          format.html { redirect_to teacher_lessons_url, :flash => { :danger => "Data deve ser maior que a data atual." } }
      end
    elsif @lesson.date > DateTime.now + 30.days
      respond_to do |format|
          format.html { redirect_to teacher_lessons_url, :flash => { :danger => "Você não pode adicionar horários com mais de 1 mês de antecedência." } }
      end
    else     

      #verifica se tem algum horário no período selecionado
      exist_lesson = Lesson.where("date > ? and date < ? and teacher_id = ? ", @lesson.date - 30.minutes, @lesson.date + 30.minutes, @lesson.teacher_id).count

      if exist_lesson > 0 then
        respond_to do |format|
          format.html { redirect_to teacher_lessons_url, :flash => { :danger => "Você não pode adicionar este horário. Ele está em conflito com outro." } }
        end
      else
        #adiciona o periodo
        ini_date_dawn = @lesson.date.change({:min => 0, :hour => 0, :sec => 0})
        end_date_dawn = @lesson.date.change({:min => 59, :hour => 5, :sec => 0})
        ini_date_morning = @lesson.date.change({:min => 59, :hour => 5, :sec => 0})
        end_date_morning = @lesson.date.change({:min => 59, :hour => 11, :sec => 0})
        ini_date_afternoon = @lesson.date.change({:min => 59, :hour => 11, :sec => 0})
        end_date_afternoon = @lesson.date.change({:min => 59, :hour => 17, :sec => 0})
        ini_date_night = @lesson.date.change({:min => 59, :hour => 17, :sec => 0})
        end_date_night = @lesson.date.change({:min => 59, :hour => 23, :sec => 0})
  
        if @lesson.date >= ini_date_dawn and @lesson.date < end_date_dawn then
          @lesson.period = WEE_CONFIG['DAWN']
        elsif @lesson.date > ini_date_morning and @lesson.date < end_date_morning then
          @lesson.period = WEE_CONFIG['MORNING']
        elsif @lesson.date > ini_date_afternoon and @lesson.date < end_date_afternoon then
          @lesson.period = WEE_CONFIG['AFTERNOON']
        elsif @lesson.date > ini_date_night and @lesson.date < end_date_night then
          @lesson.period = WEE_CONFIG['NIGHT']
        end
        
        respond_to do |format|
          if @lesson.save
            format.html { redirect_to teacher_lessons_url, :flash => { :success => "Horário adicionado." } }
          else
            format.html { redirect_to teacher_lessons_url, :flash => { :danger => "Horário adicionado." } }
          end
        end  
      end
    end

  end
  
  def destroy
    @lesson.destroy
    respond_to do |format|
      format.html { redirect_to teacher_lessons_url, notice: 'Teacher was successfully destroyed.' }
    end
  end

  private
    def set_lesson
      @lesson = Lesson.where(:id => params[:id]).where(:teacher_id => params[:teacher_id]).first
    end
    def lesson_params
      params.require(:lesson).permit(:date, :teacher_id, :language_id, :level_id)
    end
end
