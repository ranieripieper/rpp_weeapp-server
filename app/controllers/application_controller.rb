class ApplicationController < ActionController::Base
  skip_before_filter :verify_signed_out_user
  protect_from_forgery with: :null_session

  def after_sign_in_path_for(resource)
    request.env['omniauth.origin'] || stored_location_for(resource) || teachers_path
  end

  def authenticate_user_from_token!
    if request.headers['HTTP_ID'].present?
      user_id = request.headers['HTTP_ID']
      auth_token = request.headers['HTTP_AUTH_TOKEN']
    else
      user_id = params[:id].presence
      auth_token = params[:auth_token].presence
    end

    user       = User.find_by_id(user_id)

    if user && Devise.secure_compare(user.auth_token, auth_token)
      sign_in user, store: false
    else
      return render status: 401, json: {errors: ['login not found']}
    end

  end

end
