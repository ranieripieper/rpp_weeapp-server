json.array!(@platforms) do |platform|
  json.extract! platform, :id, :name
end
