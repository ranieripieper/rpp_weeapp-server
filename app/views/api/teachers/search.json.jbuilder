json.teachers @teachers do |teacher| 
  if teacher.rate_numbers == 0
    json.extract! teacher, :id, :name, :rate, :avatar
  else
    json.extract! teacher, :id, :name, :rate, :avatar
  end
  json.languages teacher.languages, :id, :name
  json.levels teacher.levels, :id, :name
  json.platforms teacher.platforms, :id, :name
  json.schedules teacher.schedules, :id, :name
  json.url api_teacher_url(teacher, format: :json)
end

json.pagination @total do |pagination|
  json.extract! pagination, :page, :total
end
