json.extract! @teacher, :id, :name, :rate, :avatar

json.languages @teacher.languages, :id, :name
# json.levels @teacher.levels, :id, :name
json.teacher_languages @teacher.teacher_languages

json.platforms @teacher.platforms, :id, :name
json.schedules @teacher.schedules, :id, :name

# json.lessons @teacher.lessons, :id, :date, :schedule