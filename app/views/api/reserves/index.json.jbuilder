json.array!(@reserves) do |reserve|
  json.extract! reserve, :id, :lesson_id, :rate, :orderpay_id
  
  json.extract! reserve.lesson, :date
  # json.date reserve.lesson, :date
  json.language reserve.lesson.language, :id, :name
  json.teacher reserve.lesson.teacher, :id, :name
  # json.platform reserve.lesson.platform, :id, :name
  json.level reserve.lesson.level, :id, :name
  json.platforms reserve.lesson.teacher.teacher_platforms, :platform, :contact
  
  # json.schedule reserve.lesson.schedule, :id, :name
end


