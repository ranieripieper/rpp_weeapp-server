json.tips @tips do |tip| 
  json.extract! tip, :id, :description, :created_at
end

json.pagination @total do |pagination|
  json.extract! pagination, :page, :total
end
