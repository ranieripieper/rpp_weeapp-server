json.array!(@users) do |user|
  json.extract! user, :id, :name, :email

  if user.avatar.present?
    json.avatar user.avatar
  end
end
