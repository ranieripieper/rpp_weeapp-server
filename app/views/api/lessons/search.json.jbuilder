json.array!(@lessons) do |lesson|
  json.extract! lesson, :id, :date
  
  json.array!(lesson.language) do |lang|
    json.extract! lang.language, :id, :name
    json.level lang.levels, :id, :name
  end
  json.level lesson.language.levels, :id, :name
  json.teacher lesson.teacher, :id, :name
  json.platform lesson.platform, :id, :name
  # json.level lesson.level, :id, :name
  json.schedule lesson.schedule, :id, :name
end
