json.array!(@lessons) do |lesson|
  json.extract! lesson, :id, :date
  json.teacher lesson.teacher, :id, :name
  # json.platform lesson.platform, :id, :name
  if lesson.language.present?
    json.language lesson.language, :id, :name
  end
  if lesson.level.present?
    json.level lesson.level, :id, :name
  end
  # json.schedule lesson.schedule, :id, :name
end
