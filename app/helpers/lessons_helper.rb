module LessonsHelper

  def format_lesson lesson_id, language_id
    
    lesson_ = Lesson.find(lesson_id)

    if lesson_.level.present?
      lesson_.level.name
    end
  end
end

