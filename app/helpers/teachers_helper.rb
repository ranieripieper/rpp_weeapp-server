module TeachersHelper

  def find(id)
    @language_ids.each{|l| 
      if l.keys.include?(id) == true
        return true
      end
    }
    return false
  end

  def get_lang(id)
    @language_ids.each{|l| 
      if l.keys.include?(id) == true
        return l
      end
    }
    return nil
  end

end
