class ServiceColumns

  def self.tip
    {
      except: [:updated_at]
    }
  end
  
  def self.level
    {
      only: [:id, :name],
    }
  end

  def self.teacher
    {
      methods:[:avatarurl],
      only: [:id, :name, :rate, :skype, :hangout]
    }
  end

  def self.lesson
    {
      only: [:id, :date],
      include: {
        teacher: ServiceColumns.teacher,
        level: ServiceColumns.level,
        language: ServiceColumns.language,
      }
    }
  end
  
  def self.next_old_lesson
    {
      only: [:id, :date],
      include: {
        teacher: ServiceColumns.teachers,
        level: ServiceColumns.level,
        language: ServiceColumns.language,
        reservelesson: ServiceColumns.basic_reserves
      }
    }
  end


  def self.teachers_next_old_lesson
    {
      only: [:id, :name, :rate, :skype, :hangout],
      methods: [:avatarurl],
      include: {
        teacher_languages: ServiceColumns.teacher_languages,
        teacher_lessons: ServiceColumns.next_old_lesson
      }
    }
  end
  
  def self.basic_reserves
    {
      only: [:id, :rate]
    }
  end
  
  def self.reserves
    {
      only: [:id, :rate],
      include: {
        lesson: ServiceColumns.lesson,
      }
    }
  end

  def self.levels
    {
      only: [:id, :name]
    }
  end
  
  def self.levels
    {
      only: [:id, :name]
    }
  end

  def self.language_levels
    { 
      only: [:id],
      include: {
        level: ServiceColumns.levels,
      }
    }
  end

  def self.language
    {
      only: [:id, :name],
    }
  end

  def self.teacher_languages
    {
      only: [:id, :language_id],
      include: {
        language: ServiceColumns.language,
        language_levels: ServiceColumns.language_levels,
      }
    }
  end

  def self.languages
    {

      only: [:id, :name]
      
    }
  end


  def self.teachers
    {
      only: [:id, :name, :rate, :skype, :hangout],
      methods: [:avatarurl],
      include: {
        teacher_languages: ServiceColumns.teacher_languages
      }
    }
  end
end