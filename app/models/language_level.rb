class LanguageLevel < ActiveRecord::Base
  belongs_to :teacher_language
  belongs_to :level
end
