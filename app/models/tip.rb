class Tip < ActiveRecord::Base
  validates :description, length: { minimum: 8, maximum: 250 }
end
