class Language < ActiveRecord::Base
  has_many :teacher_languages
  has_many :teachers, through: :teacher_languages

  has_many :lessons

end
