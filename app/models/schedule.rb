class Schedule < ActiveRecord::Base
  has_many :teacher_schedules
  has_many :teachers, through: :teacher_schedules
end
