class Reservelesson < ActiveRecord::Base
  acts_as_paranoid
  
  belongs_to :user
  belongs_to :lesson
  belongs_to :orderpay

  def rate_teacher(value)

    self.update_attributes(:rate => value)

    teacher = self.lesson.teacher

    new_rate_numbers = Reservelesson.joins(:lesson).joins(:lesson =>:teacher).where("reservelessons.rate > ? and teachers.id = ? ", 0, teacher.id).count
    new_rate = Reservelesson.joins(:lesson).joins(:lesson =>:teacher).where("reservelessons.rate > ? and teachers.id = ? ", 0, teacher.id).sum("reservelessons.rate")
    
    new_rate = new_rate / new_rate_numbers
    teacher.update_attributes(:rate => new_rate, :rate_numbers => new_rate_numbers)

  end
end
