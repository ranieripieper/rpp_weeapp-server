class TeacherLevel < ActiveRecord::Base
  belongs_to   :teacher
  belongs_to   :level
  has_one      :lesson
end
