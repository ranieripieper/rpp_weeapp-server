class TeacherLanguage < ActiveRecord::Base
  belongs_to   :teacher
  belongs_to   :language
  has_one      :lesson
  has_many :language_levels
  belongs_to :language
  belongs_to :level

  def ids
    return LanguageLevel.select(:id).where(:teacher_languages_id => self.id)
  end

  def levels
    language_levels = LanguageLevel.joins(:level).where(:teacher_languages_id => self.id)
    levels = []
    language_levels.each do |level| 
      levels << level.level
    end

    return levels
  end
end
