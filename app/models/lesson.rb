class Lesson < ActiveRecord::Base
  acts_as_paranoid
  belongs_to :teacher
  has_one :reservelesson
  
  belongs_to :teacher_language
  belongs_to :teacher_level
  belongs_to :teacher_schedule
  belongs_to :teacher_platform

  belongs_to :language
  belongs_to :level
  
  has_one :schedule, through: :teacher_schedule
  has_many :platforms, through: :teacher_platform

  scope :empty, -> { select(:id).joins("LEFT JOIN reservelessons ON lessons.id = reservelessons.lesson_id WHERE reservelessons.lesson_id IS NULL") }

  # Temp ... NAO USADO 
  def self.search(platform = nil, level = nil, language = nil, schedule = nil)
    
    lessons = Lesson.empty

    ids = []
    lessons.all.each do |appointment| 
      ids << appointment.id
    end

    lessons = Lesson.where({id:ids} )

    if platform.present?
      platform = platform.include?(",") ? platform.split(",") : platform
      lessons = lessons.joins(:platform).where(platforms: {id: platform}) unless platform.blank?
    end

    if level.present?
      level = level.include?(",") ? level.split(",") : level
      lessons = lessons.joins(:level).where(levels: {id: level}) unless level.blank?
    end

    if language.present?
      language = language.include?(",") ? language.split(",") : language
      lessons = lessons.joins(:language).where(languages: {id: language})
    end  
    
    if schedule.present?
      schedule = schedule.include?(",") ? schedule.split(",") : schedule
      lessons = lessons.joins(:schedule).where(schedules: {id: schedule})
    end  

    return lessons
  end

end
