class Level < ActiveRecord::Base
  has_many :teacher_levels
  has_many :teachers, through: :teacher_levels
  has_many :lessons

  belongs_to :teacher_language
end
