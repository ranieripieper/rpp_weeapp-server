class Orderpay < ActiveRecord::Base
  has_many :reservelessons

  # status = 1 pago
  NOTPAID = 0
  PAID = 1
  WAITING = 2
  
  STATUS = ['nao pago', 'pago', 'aguardando']
end
