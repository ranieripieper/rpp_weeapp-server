class TeacherSchedule < ActiveRecord::Base
  belongs_to   :teacher
  belongs_to   :schedule
  has_one      :lesson
end
