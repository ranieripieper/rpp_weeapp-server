class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :reservelessons
  
  before_save :ensure_authentication_token!

  def upload_avatar(filename)
    require 'aws/s3'

    bucket_name = WEE_CONFIG['AWS_BUCKET']

    @s3_client ||=  AWS::S3.new(
      :access_key_id     =>  WEE_CONFIG['AWS_ACCESS_KEY_ID'],
      :secret_access_key =>  WEE_CONFIG['AWS_SECRET_ACCESS_KEY']
    )
    bucket      = @s3_client.buckets[bucket_name]
    bucket      = @c3_client.buckets.create(bucket_name) unless (bucket.exists?)

    file = filename

    filename = File.join('users/avatar/', self.id.to_s + '.jpg')
    s3_file = bucket.objects[filename]

    avatar_file = s3_file.write file , acl: :public_read

    avatar_url        = avatar_file.public_url.to_s
    # avatar_url      = avatar_file.url_for(:read).to_s
    self.update_attribute(:avatar , avatar_url)
    avatar_url
  end

  def ensure_authentication_token!
    if auth_token.blank?
      self.auth_token = generate_authentication_token
    end
  end
  
  def generate_new_token
    self.auth_token = generate_authentication_token
    self.save
  end

  private
  
  def generate_authentication_token
    loop do
      auth_token = Devise.friendly_token
      break auth_token unless User.where(auth_token: auth_token).first
    end
  end
end
