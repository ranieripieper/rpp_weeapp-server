class Teacher < ActiveRecord::Base
  has_attached_file :avatar, styles: { :mobile => "300x300>" }, :default_url => "#{WEE_CONFIG['ROOT_URL']}/assets/avatar/small/missing.png"
  validates_attachment_content_type :avatar, :content_type => /^image\/(png|gif|jpeg|jpg)/
  
  attr_accessor :teacher_lessons
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  # has_many :levels, through: :teacher_languages
  
  has_many :teacher_languages
  has_many :languages, through: :teacher_languages

  # has_many :levels, through: :languages

  has_many :teacher_schedules
  has_many :schedules, through: :teacher_schedules

  has_many :lessons
  
  has_many :reservelessons, through: :lessons
  
  def avatarurl
    avatarurl = self.avatar.url(:mobile)

    if avatarurl.blank?
      return ""
    end
    return avatarurl
  end

  def self.search(skype = nil, hangout = nil, level = nil, language = nil, schedule = nil, page = 1, per_page = 10)
    teachers = Teacher
                .joins(:lessons)
                .where("(lessons.id NOT IN (SELECT reservelessons.lesson_id FROM reservelessons where reservelessons.deleted_at IS  NULL))")
                .where("lessons.date > ? ", DateTime.now)
            
    language_join = false    
    if not language.nil? and not language.blank? then
      language_join = true
      teachers = teachers.joins(:teacher_languages).where("teacher_languages.language_id = ?", language)
    end

    if not level.nil? and not level.blank? then
      if not language_join then
        teachers = teachers.joins(:teacher_languages)
      end
      teachers = teachers.joins(:teacher_languages => :language_levels).where("language_levels.level_id = ?", level)
    end
    
    if not skype.nil? and not skype.blank? then
      teachers = teachers.where("skype is not null and skype != ? ", "")
    end  
    if not hangout.nil? and not hangout.blank? then
      teachers = teachers.where("hangout is not null and hangout != ? ", "")
    end
    
    if not schedule.nil? and not schedule.blank? then
      teachers = teachers.where("lessons.period in (?) ", schedule.split(","))
    end
    
    return teachers.distinct.paginate(:page => page, :per_page => per_page)

  end
end
