require 'rails_helper'

RSpec.describe "tips/show", type: :view do
  before(:each) do
    @tip = assign(:tip, Tip.create!(
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
  end
end
