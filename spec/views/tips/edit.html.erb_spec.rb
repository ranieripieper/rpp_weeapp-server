require 'rails_helper'

RSpec.describe "tips/edit", type: :view do
  before(:each) do
    @tip = assign(:tip, Tip.create!(
      :description => "MyText"
    ))
  end

  it "renders the edit tip form" do
    render

    assert_select "form[action=?][method=?]", tip_path(@tip), "post" do

      assert_select "textarea#tip_description[name=?]", "tip[description]"
    end
  end
end
