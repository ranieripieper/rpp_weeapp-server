require 'rails_helper'

RSpec.describe "tips/new", type: :view do
  before(:each) do
    assign(:tip, Tip.new(
      :description => "MyText"
    ))
  end

  it "renders new tip form" do
    render

    assert_select "form[action=?][method=?]", tips_path, "post" do

      assert_select "textarea#tip_description[name=?]", "tip[description]"
    end
  end
end
