require 'rails_helper'

RSpec.describe "tips/index", type: :view do
  before(:each) do
    assign(:tips, [
      Tip.create!(
        :description => "MyText"
      ),
      Tip.create!(
        :description => "MyText"
      )
    ])
  end

  it "renders a list of tips" do
    render
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
